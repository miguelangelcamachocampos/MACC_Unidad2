<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="m" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Custumers list</title>
</head>
<body>
	<table border="1">
		<tr>
			<th>
			  <form action="CustumerController">
			  		<input type="submit" name="btn_new"
			  		value="New "/>
			  </form>
			</th>
			<td>Id</td>
			<td>CompanyName</td>
			<td>ContactName</td>
			<td>ContactTitle</td>
		</tr>
		 <m:forEach var="custumer" items="${custumers}">
		 	 <tr>
		 	 	<td>
		 	 		<form action="CustumerController">
		 	 			<input type="hidden" name="id" value="${custumer.id }">
		 	 			<input type="submit" name="btn_edit" value="Edit"/>
		 	 			 <input type="submit" name="btn_delete" value="Delete"/>
		 	 		</form>
		 	 	</td>
		 	 	<td>${custumer.id }</td>
		 	 	<td>${custumer.company }</td>
		 	 	<td>${custumer.name }</td>
		 	 	<td>${custumer.title }</td>
		 	 </tr>	
		 </m:forEach>
	</table>
</body>
</html>