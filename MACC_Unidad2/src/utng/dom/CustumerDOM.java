package utng.dom;

import java.util.List;
import java.util.ArrayList;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import utng.model.Custumer;


public class CustumerDOM {
	private String pathFile = "C:\\Users\\usuario\\eclipse-workspace\\Proyect3\\src\\data\\students.xml";

	public void add(Custumer data) {
		try {
			Document document = DOMHelper.getDocument(pathFile);
			Element custumers = document.getDocumentElement();
			// Create student tag
			Element custumer = document.createElement("custumer");
			// Create id tag
			Element id = document.createElement("id");
			id.appendChild(document.createTextNode(data.getId()));
			custumer.appendChild(id);
			
			// Create name tag
			Element company = document.createElement("company");
			company.appendChild(document.createTextNode(data.getCompany()));
			custumer.appendChild(company);

			// Create age tag
			Element contact = document.createElement("contact");
			contact.appendChild(document.createTextNode(String.valueOf(data.getContact())));
			custumer.appendChild(contact);
	
			
			// Create age tag
			Element title = document.createElement("title");
			title.appendChild(document.createTextNode(String.valueOf(data.getTitle())));
			custumer.appendChild(title);
				
			custumers.appendChild(custumer);
			// Write to file
			DOMHelper.saveXMLContent(document, pathFile);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void delete(String id) {
		try {
			Document document = DOMHelper.getDocument(pathFile);
			NodeList nodeList = document.getElementsByTagName("custumer");
			for (int i = 0; i < nodeList.getLength(); i++) {
				Element custumer = (Element) nodeList.item(i);
				if (custumer.getElementsByTagName("id").item(0).getTextContent().equals(id)) {
					custumer.getParentNode().removeChild(custumer);
				}
			}
			DOMHelper.saveXMLContent(document, pathFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void update(Custumer data) {
		try {
			Document document = DOMHelper.getDocument(pathFile);
			NodeList nodeList = document.getElementsByTagName("custumer");
			for (int i = 0; i < nodeList.getLength(); i++) {
				Element custumer = (Element) nodeList.item(i);
				if (custumer.getElementsByTagName("id").item(0).getTextContent().equals(data.getId())) {
					custumer.getElementsByTagName("company").item(0).setTextContent(data.getCompany());
					custumer.getElementsByTagName("contact").item(0).setTextContent(String.valueOf(data.getContact()));
					custumer.getElementsByTagName("title").item(0).setTextContent(String.valueOf(data.getTitle()));
				}
			}
			DOMHelper.saveXMLContent(document, pathFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Custumer findById(String id) {
		Custumer custumer = null;
		try {
			Document document = DOMHelper.getDocument(pathFile);
			NodeList nodeList = document.getElementsByTagName("custumer");
			for (int i = 0; i < nodeList.getLength(); i++) {
				Element c = (Element) nodeList.item(i);
				if (c.getElementsByTagName("id").item(0).getTextContent().equals(id)) {
					custumer = new Custumer();
					custumer.setId(id);
					custumer.setCompany(c.getElementsByTagName("company").item(0).getTextContent());
					custumer.setContact(c.getElementsByTagName("contact").item(0).getTextContent());
					custumer.setTitle(c.getElementsByTagName("title").item(0).getTextContent());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();

		}
		return custumer;
	}

	public List<Custumer> getCutumers() {
		List<Custumer> custumers = new ArrayList<Custumer>();
		Document document = DOMHelper.getDocument(pathFile);
		NodeList nodeList = document.getElementsByTagName("custumer");
		for(int i=0; i<nodeList.getLength(); i++) {
			Element c = (Element)nodeList.item(i);
			Custumer s = new Custumer();
			
			
			
			
			
			
			
			
			
			
			
			
			custumers.setId(c.getElementsByTagName("id").item(0).getTextContent());
			custumers.setName(s.getElementsByTagName("name").item(0).getTextContent());
			custumers.setAge(Integer.parseInt(s.getElementsByTagName("age").item(0).getTextContent()));
			custumers.add(Custumer);
		}
		return custumers;
	}
}
