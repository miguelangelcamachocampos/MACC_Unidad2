package utng.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import utng.dom.CustumerDOM;
import utng.model.Custumer;


/**
 * Servlet implementation class CarController
 */
@WebServlet("/CustumerController")
public class CustumerController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Custumer custumer;
    private java.util.List<Custumer> custumers; 
	private CustumerDOM custumerDOM;
	private List<String> ids = new ArrayList<String>();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CustumerController() {
        super();
        custumer = new Custumer();
        custumers = new java.util.ArrayList<Custumer>();
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getParameter("btn_save")!=null) {
			custumer.setCompany(request.getParameter("company"));
			custumer.setContact(request.getParameter("name"));
			custumer.setTitle(request.getParameter("title"));
		
			
		//car.setColor(request.getParameter("color"));
		if(custumer.getId()=="") {
		//custumer.setId(Long.parseLong(String.valueOf(custumer.size()+1)));
		custumers.add(custumer);

		
		}
		request.setAttribute("custumers", custumers);
		request.getRequestDispatcher("custumer_list.jsp").forward(request, response);
	}
	
		if(request.getParameter("btn_new")!=null) {
			custumer = new Custumer();
			request.getRequestDispatcher("custumer_form.jsp").forward(request, response);
		}
		if(request.getParameter("btn_edit")!=null) {
			try {
				//Long id=Long.parseLong(request.getParameter("id"));
				for	(Custumer c: custumers) {
					if(c.getId()=="") {
						custumer = c;
						break;
					}
				}
			}catch (NumberFormatException e) {
				custumer = new Custumer();
			}

			request.setAttribute("custumer", custumer);
			request.getRequestDispatcher("car_form.jsp").forward(request, response);
		}
		if(request.getParameter("btn_delete")!=null){
			try {
				//Long id = Long.parseLong(request.getParameter("id"));
				for	(Custumer c: custumers) {
					if(c.getId()=="") {
						custumer = c;
						//car = c;
					}
				}
				custumers.remove(custumer);
				//cars.remove(car);
			}catch (IndexOutOfBoundsException e){
				System.out.println(custumers);
				//custumer.remove(0);
			}
			request.setAttribute("custumer", custumers);
			request.getRequestDispatcher("car_list.jsp").forward(request, response);
		}
		
}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
