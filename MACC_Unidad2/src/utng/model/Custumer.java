/**
 * 
 */
package utng.model;

import java.io.Serializable;

/**
 * @author usuario
 *
 */
public class Custumer implements Serializable{
	private String id;
	private String company;
	private String contact;
	private String title;
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the company
	 */
	public String getCompany() {
		return company;
	}
	/**
	 * @param company the company to set
	 */
	public void setCompany(String company) {
		this.company = company;
	}
	/**
	 * @return the contact
	 */
	public String getContact() {
		return contact;
	}
	/**
	 * @param contact the contact to set
	 */
	public void setContact(String contact) {
		this.contact = contact;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Custumer [id=" + id + ", company=" + company + ", contact=" + contact + ", title=" + title + "]";
	}

	public Custumer() {
	//	("","","","");
	}
	public Custumer(String string, String string2, String string3, String string4) {
		// TODO Auto-generated constructor stub
	}
	
	
	
}
